# NER_tweets

This project uses Bi-directional LSTM to identify named entity such as names of people, organizations and locations, inside tweets.


# Dataset
The dataset is made of less than 10 000 tweets which covers about 20 named entities.
The dataset has already been split in train, validation and test sets in the *data* folder


# Usage

## Clone this repository
```
git clone https://gitlab.com/devoteam-gcloud-public/gcp-ml-public-demos.git
```

## Create a virtual environment with
- python 3
- tensorflow 1.12
- numpy

### Use any tool you are used to. Example with *conda*
```
conda create -n ner_tweets python=3.5
source activate ner_tweets
```

### Install the required libraries
```
cd NER_tweets
pip install -r requirements.txt
```

## Run the notebook
Then inside your environment, open the notebook *NER.ipynb* in a notebook of your choice.

For instance, you can use:
- [jupyter notebook](https://jupyter.org/)
- [google colaboratory](https://colab.research.google.com/notebooks/basic_features_overview.ipynb)

Then follow the instructions inside the notebook


# Contact
- marc.djohossou@devoteamgcloud.com
- alejandra.paredes@devoteamgcloud.com